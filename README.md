## Getting started
# TryHackMe Advent of Cyber 2023

Welcome to the TryHackMe Advent of Cyber 2023 repository! This repository contains solutions and resources for the challenges presented in the Advent of Cyber security training by TryHackMe.

## Table of Contents
- [About Advent of Cyber](#about-advent-of-cyber)
- [Getting Started](#getting-started)
- [Challenges](#challenges)
- [License](#license)

## About Advent of Cyber
Advent of Cyber is a yearly cybersecurity training event hosted by TryHackMe. It consists of daily challenges and tasks designed to improve your penetration testing and ethical hacking skills. Each day covers a new topic, building up to a comprehensive understanding of various cybersecurity concepts.

## Getting Started
To get started with Advent of Cyber 2023, follow these steps:

1. Clone this repository:
   ```bash
   git clone https://gitlab.com/your-username/advent-of-cyber-2023.git
   cd advent-of-cyber-2023u

Navigate to the challenge directory for each day to find the challenges and resources.

Use the provided solutions, walkthroughs, and resources to complete the challenges.

## Challenges
The challenges are organized by day, and each day covers a specific topic or skill. You can find the challenges in the respective directories.

* Day 1: Machine learning Chatbot, tell me, if you're really safe?
* Day 2:  Log analysis O Data, 
* Day 3:  Brute-forcing Hydra i
* Day 4:  Brute-forcing Baby, 
* Day 5:  Reverse engineering 
* Day 6:  Memory corruption
* Day 7:  Log Analysis
* Day 8:  Disk Forensics
* Day 9:  Malware Analysis
* Day 10: SQL Injection
* Day 11: Active Directory
* Day 12: Defence in Depth
* Day 13: Intrusion Detection
* Day 14: Machine Learning
* Day 15: Machine Learning
* Day 16: Machine Learning
* Day 17: Traffic Analysis
* Day 18: Eradication
* Day 19: Memory Forensics
* Day 20: DevSecOps
* Day 21: DevSecOps
* Day 22: SSRF
* Day 23: Coerced Authentication
* Day 24: Mobile Analysis
* Day 24: The Confrontation
* Day 24: The End
...
Feel free to explore and attempt the challenges at your own pace.

***
## License
This project is licensed under the MIT License, which means you are free to use, modify, and distribute the code and resources. However, please attribute TryHackMe and the respective authors of the challenges when sharing or using this material.

Happy hacking!

